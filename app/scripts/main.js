$(document).ready(function () {
  // Llamada a la libreria de accordion-wizard
  /* $('.acc-wizard').accwizard(); */
    var altoHeight = $(window).height();
    var windowHeight = $(window).scrollTop();
    var menuWizard = $('.menu-wizard');
    var contenidoWizard = $('.contenido-wizard');

    // var altomenu = menuWizard.offset();
    // altomenu = altomenu.top;
    if (windowHeight >= 70) {
      // console.log('estas dentro');
      menuWizard.addClass('fijado');
      contenidoWizard.addClass('fijado');
      // contenidoWizard.css('padding-top','30px');
    } else {
      //console.log('saliste');
      menuWizard.removeClass('fijado');
      contenidoWizard.removeClass('fijado');
      // contenidoWizard.css('padding-top','0px');
    }
  $(window).scroll(function () {
    var altoHeight = $(window).height();
    var windowHeight = $(window).scrollTop();
    var menuWizard = $('.menu-wizard');
    var contenidoWizard = $('.contenido-wizard');

    // var altomenu = menuWizard.offset();
    // altomenu = altomenu.top;
    if (windowHeight >= 70) {
      // console.log('estas dentro');
      menuWizard.addClass('fijado');
      contenidoWizard.addClass('fijado');
      // contenidoWizard.css('padding-top','30px');
    } else {
      //console.log('saliste');
      menuWizard.removeClass('fijado');
      contenidoWizard.removeClass('fijado');
      // contenidoWizard.css('padding-top','0px');
    }

  });

  // Funcionalidad para la visualización del ascensor
  if ($(this).scrollTop() > 100) {
      $('.GoTop').fadeIn();
  } else {
      $('.GoTop').fadeOut();
  }
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('.GoTop').fadeIn();
    } else {
       $('.GoTop').fadeOut();
    }
   });

  // Funcionalidad para poder cerrar el accordion al pulsar sobre una opción del menu

  // Funcionalidad para subir con el ascensor

  $('.GoTop').click(function(){
    $('html, body').animate({ scrollTop: 0 }, 600);
    //return false;
  });

  // Funcionalidad para seleccionar una opción del menu-responsive y navegar hasta el panel correspondiente
  $('.menu-responsive ol li a').on('click',function(e){  //Step 1
    var $valorOpcion = $(this).attr('title');
    // alert($valorOpcion);
    $('html,body').animate({
      scrollTop: $('#c'+$valorOpcion).parent().parent().offset().top - 60
    },0);
    // $('.navbar-collapse').collapse('hide');
    $('img.navbar-toggle').trigger('click');
    // return false;
    if($('#step'+$valorOpcion).hasClass('in')) {
      e.preventDefault();
      e.stopPropagation();
    }
  });

  // Simulación de paso guardado al pulsar sobre imagen "disquete"

  $('span.imagen-guardar').on('click', function() {
    var $valorId = $(this).attr('id');
    // alert($valorId);
    $('#'+$valorId+'.imagen-guardar').css({'background-image': 'url(./images/cargando.gif)'});
    setTimeout(function (){
      //var $valorId = $(this).attr('id');
      $('#'+$valorId+'.imagen-guardar').css({'background-image': 'url(./images/icono-guardar.png)'});
      $('#p'+$valorId).addClass('acc-wizard-active');
      $('#p'+$valorId+'-responsive').addClass('acc-wizard-active');
      $('#c'+$valorId).addClass('save');
    }, 2000);
  });

  // simulación de validaciones

  $('#generar-propuesta-2a').on('click', function() {
    var $imagenCargando = $(this).find('img');
    $imagenCargando.removeClass('hidden');
    var $cargandoCapa = $(this).closest('.panel').find('.container-loading');
    $cargandoCapa.addClass('on');
    setTimeout(function () {
      $cargandoCapa.removeClass('on')
      $('.content-alertas').removeClass('hidden');
      $imagenCargando.addClass('hidden');
    }, 2000);
  });

  $('#generar-propuesta-7').on('click', function() {
    var $imagenCargando = $(this).find('img');
    $imagenCargando.removeClass('hidden');
    var $cargandoCapa = $(this).closest('.panel').find('.container-loading');
    $cargandoCapa.addClass('on');
    setTimeout(function () {
      $cargandoCapa.removeClass('on');
      $('.form-group.has-error').addClass('activate');
      $imagenCargando.addClass('hidden');
    }, 2000);

  });

  $('#abrir-todos').on('click', function() {
    $('.panel-collapse:not(".in")').collapse('show');
  });
  $('#cerrar-todos').on('click', function(){
    $('.panel-collapse.in').collapse('hide');
  });

});


