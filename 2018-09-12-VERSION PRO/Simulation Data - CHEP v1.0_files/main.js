$(document).ready(function () {

    // Condición para reestructurar los pasos del wizard dependiendo de su numero
    function colocacionPasos() {
        var $listadoPasos = $('ol.acc-wizard-sidebar');
        var $pasos = $listadoPasos.find('li');
        if ($pasos.length >= 7) {
            //alert('mayor 7');
            $listadoPasos.addClass('contraer');
        } else {
            //alert('menor 7');
        }
    }
    colocacionPasos();

    // Llamada a la libreria de accordion-wizard
    /* $('.acc-wizard').accwizard(); */
    var altoHeight = $(window).height();
    var windowHeight = $(window).scrollTop();
    var menuWizard = $('.menu-wizard');
    var contenidoWizard = $('.contenido-wizard');

    // var altomenu = menuWizard.offset();
    // altomenu = altomenu.top;
    if (windowHeight >= 150) {
        // console.log('estas dentro');
        menuWizard.addClass('fijado');
        contenidoWizard.addClass('fijado');
        // contenidoWizard.css('padding-top','30px');
    } else {
        //console.log('saliste');
        menuWizard.removeClass('fijado');
        contenidoWizard.removeClass('fijado');
        // contenidoWizard.css('padding-top','0px');
    }
    $(window).scroll(function () {
        var altoHeight = $(window).height();
        var windowHeight = $(window).scrollTop();
        var menuWizard = $('.menu-wizard');
        var contenidoWizard = $('.contenido-wizard');

        // var altomenu = menuWizard.offset();
        // altomenu = altomenu.top;
        if (windowHeight >= 150) {
            // console.log('estas dentro');
            menuWizard.addClass('fijado');
            contenidoWizard.addClass('fijado');
            // contenidoWizard.css('padding-top','30px');
        } else {
            //console.log('saliste');
            menuWizard.removeClass('fijado');
            contenidoWizard.removeClass('fijado');
            // contenidoWizard.css('padding-top','0px');
        }

    });

    // Funcionalidad para la visualización del ascensor
    if ($(this).scrollTop() > 100) {
        $('.GoTop').fadeIn();
    } else {
        $('.GoTop').fadeOut();
    }
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.GoTop').fadeIn();
        } else {
            $('.GoTop').fadeOut();
        }
    });

    // Funcionalidad para poder cerrar el accordion al pulsar sobre una opción del menu
    /* $('.menu-responsive ol li a').on('click',function() {
      $('img.navbar-toggle').trigger('click');
    }); */

    // Funcionalidad para subir con el ascensor

    $('.GoTop').click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 600);
        //return false;
    });


    // Funcionalidad para seleccionar una opción del menu-responsive y navegar hasta el panel correspondiente
    $('.menu-responsive ol li a').on('click', function (e) { //Step 1
        var $valorOpcion = $(this).attr('title');
        // alert($valorOpcion);
        $('html,body').animate({
            scrollTop: $('#c' + $valorOpcion).parent().parent().offset().top - 60
        }, 0);
        // $('.navbar-collapse').collapse('hide');
        $('img.navbar-toggle').trigger('click');
        // return false;
        if ($('#step' + $valorOpcion).hasClass('in')) {
            e.preventDefault();
            e.stopPropagation();
        }
    });

    // simulación de validaciones
    //$('#generar-propuesta-2a').on('click', function () {
    //    var $imagenCargando = $(this).find('img');
    //    $imagenCargando.removeClass('hidden');
    //    setTimeout(function () {
    //        $('.content-alertas').removeClass('hidden');
    //        $imagenCargando.addClass('hidden');
    //    }, 2000);
    //});

    //$('#generar-propuesta-7').on('click', function () {
    //    var $imagenCargando = $(this).find('img');
    //    $imagenCargando.removeClass('hidden');
    //    setTimeout(function () {
    //        $('.form-group.has-error').addClass('activate');
    //        $imagenCargando.addClass('hidden');
    //    }, 2000);

    //});

    /*
    $('#abrir-todos').on('click', function () {
        $('.panel-collapse:not(".in")').collapse('show');
    });
    $('#cerrar-todos').on('click', function () {
        $('.panel-collapse.in').collapse('hide');
    });
    */


    /**
     * Cuando se produce un cambio en cualquier input marcamos el botón de grabar como activo
     * */
    $("div[data-step] :input").on('change', function () {

        var $this = $(this);
        var $panel = $(this).closest(".panel");
        $panel.find(".btn-step").removeAttr("disabled").removeClass("error").attr("title", "Pending changes");

        $(this).closest(".has-error").removeClass("has-error");
        $panel.find(".validation[validation-for=" + $.escapeSelector($(this).attr("name")) + "]").each(function () {
            validate($panel, $(this));
        })
        if (!($panel.find(".form-group.has-error").length === 0)) {
            $panel.find(".btn-step").addClass("error").attr("title", "Validation errors");
            //$panel.find("[data-toggle='collapse'][aria-expanded='false']").click();
        }
        //ponemos comprobar este item...para ver su regex...
    });

    /**
     * comprueba la validación de un input a través del elemento span class="validation".
     * @param {any} $panel
     * @param {any} $validation
     */
    function validate($panel, $validation) {
        $validation.closest(".form-group.has-error").removeClass("has-error");

        //obtenemos los valores...necesitamos escapar el selector en el momento que tenemos el caracter !
        var valor = $panel.find("[name=" + $.escapeSelector($validation.attr("validation-for")) + "]").val();
        if (valor instanceof Array) {
            valor = valor.join(",");
        }
        var regex = $validation.attr("validation-regex");
        if (!new RegExp(regex).test(valor)) {
            $validation.closest(".form-group").addClass("has-error");
        }
    }

    $(".btn-step").on('click', function () {
        var $this = $(this);
        if ($(this).attr("disabled") === "disabled") {
            return false;
        }
        var step = $(this).attr("data-step");

        /* realizamos la validación de los elementos de este
         * * */
        var $panel = $(this).closest(".panel");

        $panel.find(".form-group").removeClass("has-error");
        $panel.find(".row:visible .validation[validation-regex]").each(function () {
            validate($panel, $(this));
        });
        //comprobamos si pasa las validaciones..
        //if (!($panel.find(".validation:visible").length === 0)) {
        if (!($panel.find(".form-group.has-error").length === 0)) {
            $this.addClass("error");
            $this.attr("title", "Validation errors.");

            $panel.find("[data-toggle='collapse'][aria-expanded='false']").click();
            return false;
        }

        /**var valorId = $(this).attr('id');
        $('#' + valorId + '.imagen-guardar').css({ 'background-image': 'url(../../images/cargando.gif)' });
        */
        $this.addClass("loading").attr("title", "Generating proposal").attr("disabled", "disabled");
        //$(this).css({ 'background-image': 'url(../../images/cargando.gif)' });

        $panel.find("div.loading").removeClass("hidden");
        //enviamos por post los datos de la pestaña en cuestión.
        $.ajax({
            type: "POST",
            url: $(this).attr("data-url"),
            content: "application/json; charset=utf-8",
            dataType: "json",
            data: $("div[data-step='" + step + "'] :input:visible").serialize(),
            success: function (d) {

                //$("#" + step + ".imagen-guardar").css({ "background-image": "url(../../images/icono-guardar.png)" });
                var res = $.parseJSON(d);
                if (res.success === true) {
                    /**
                     * si el resultado ha sido correcto marcamos el botón como deshabilitado
                     * */
                    $this.attr("disabled", "disabled").removeAttr("title");

                    $('[data-saved="collapse"][data-step=' + step + ']').closest('li').addClass("acc-wizard-todo");
                    $('#c-' + step).addClass('save');
                    //marcamos el menu como todo
                } else {
                    //marcamos el botón de guardar como "error"
                    $this.addClass("error");

                    //habilitamos las validaciones de los errores que se han producido..
                    if (res.validations !== undefined) {
                        var validations = res.validations.split(",");
                        for (var k = 0; k < validations.length; k++) {
                            $panel.find(".validation[validation-for=" + $.escapeSelector(validations[k]) + "]").closest(".form-group").addClass("has-error");
                        }

                        $this.attr("title", "Validation errors.");
                        //$panel.find("[data-toggle='collapse'][aria-expanded='false']").click();
                    }
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                $this.addClass("error");
                $this.attr("title", errorThrown);
            },
            complete: function () {
                $this.removeClass("loading");
                $panel.find("div.loading").addClass("hidden");

            },
        }); //.ajax
        return false;
    });

    //// Simulación de paso guardado al pulsar sobre imagen "disquete"

    //$('span.imagen-guardar').on('click', function () {
    //    var $valorId = $(this).attr('id');
    //    // alert($valorId);
    //    $('#' + $valorId + '.imagen-guardar').css({ 'background-image': 'url(../../images/cargando.gif)' });
    //    setTimeout(function () {
    //        //var $valorId = $(this).attr('id');
    //        $('#' + $valorId + '.imagen-guardar').css({ 'background-image': 'url(../../images/icono-guardar.png)' });
    //        $('#p' + $valorId).addClass('acc-wizard-active');
    //        $('#p' + $valorId + '-responsive').addClass('acc-wizard-active');
    //        $('#c' + $valorId).addClass('save');
    //    }, 2000);
    //});


    //$(".btn-step").click(function (e) {
    //    e.preventDefault();
    //    //enviamos por post los datos de la pestaña en cuestión.
    //    var step = $(this).attr("data-step");
    //    $.ajax({
    //        type: "POST",
    //        url: $(this).attr("data-url"),
    //        content: "application/json; charset=utf-8",
    //        dataType: "json",
    //        data: $("div[data-step='" + step + "'] :input").serialize(),          
    //        success: function (d) {
    //            var res = $.parseJSON(d);
    //            if (res.success == true)
    //            {
    //                alert('success'); //disabled button. porque ya se ha grabado..                    
    //            }                  
    //            else {
    //                alert('failed'); //dejamos el boton como está..                                                      
    //            }  
    //        },
    //        error: function (xhr, textStatus, errorThrown) {
    //            // TODO: Show error
    //        }
    //    });      
    //    return false;
    //});
    $('.btn-page').on('click', function () {
        $form = $(this).closest("form");
        $form.find(':input[name=Page]').val($(this).attr('page'));
        $form.submit();
        return false;
    });
    $('.btn-pagesize').on('change', function () {
        $(this).closest("form").submit();
        return false;
    });
    $('.btn-search').on('change', function () {
        $(this).closest("form").submit();
        return false;
    });

    $(".btn-generar").on('click', function () {
        if ($(this).hasClass("cargando")) return false;

        //validar todos los campos...
        /* realizamos la validación de los elementos de este
         * * */

        //quitamos todos los errores..
        var step = $(this).attr("data-step");

        var proposal = $(this).attr("data-proposal").replace(/([^,]+)/g, ".panel-body[data-step='$1']");
        var $proposal = $(proposal);

        $proposal.find(".validation[validation-regex]").each(function () {
            var $panel = $(this).closest(".panel");
            validate($panel, $(this));
        })

        var $btn_step = $proposal.find(".form-group.has-error").closest(".panel").find(".btn-step")
        if (!($btn_step.length === 0)) {
            $btn_step.addClass("error");
            $btn_step.attr("title", "Validation errors.");
            return false;
        }

        //enviamos por post los datos de la pestaña en cuestión.
        //var step = $(this).attr("data-step");
        //var $btn_step = $(this).closest(".panel").find(".btn-step");

        $proposal.closest(".panel").find(".btn-step").addClass("loading").attr("title", "generating proposal");

        var imagen = $(this).find('img').removeClass('hidden');
        $.ajax({
            type: "POST",
            url: $(this).attr("data-url"),
            context: $(this),
            content: "application/json; charset=utf-8",
            dataType: "json",
            data: $proposal.find(":input").serialize(),
            beforeSend: function (d) {
                $(this).closest('.panel-body').find('.alert-' + step).addClass("hidden");
                $(this).closest('.panel-body').find("div.loading").removeClass("hidden");
            },
            success: function (d) {
                var res = $.parseJSON(d);
                if (res.success === true) {
                    $(this).closest('.panel-body').find('.alert-success.alert-' + step).removeClass("hidden");
                } else {
                    $(this).closest('.panel-body').find('.alert-danger.alert-' + step).find("span").html(res.message);
                    $(this).closest('.panel-body').find('.alert-danger.alert-' + step).removeClass("hidden");

                    //marcamos el botón de guardar como "error"
                    $btn_step.addClass("error");

                    //habilitamos las validaciones de los errores que se han producido..
                    if (res.validations !== undefined) {
                        var validations = res.validations.split(",");
                        for (var k = 0; k < validations.length; k++) {
                            $(this).closest(".panel").find(".validation[validation-for=" + $.escapeSelector(validations[k]) + "]").closest(".form-group").addClass("has-error");
                        }

                        $btn_step.attr("title", "Validation errors.");
                        //$(this).closest(".panel").find("[data-toggle='collapse'][aria-expanded='false']").click();
                    }

                }
            },
            complete: function (e) {
                $proposal.closest(".panel").find(".btn-step").removeClass("loading");

                imagen.addClass('hidden');
                $(this).closest('.panel-body').find("div.loading").addClass("hidden");

            },
            error: function (xhr, textStatus, errorThrown) {
                $(this).closest('.panel-body').find('.alert-danger.alert-' + step).find("span").html(errorThrown);
                $(this).closest('.panel-body').find('.alert-danger.alert-' + step).removeClass("hidden");
            }
        });
        return false;
    });

    $(document).on('click', '[data-toggle="modal"]', function (e) {

        var $this = $(this)
        var href = $this.attr('href') || $(this).attr("data-url");
        var $target = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, ''))) // strip for ie7
        var option = $target.data('bs.modal') ? 'toggle' : $.extend({
            remote: !/#/.test(href) && href
        }, $target.data(), $this.data())

        $target.find(".modal-header .titulo").html($(this).attr('data-title'));

        $target.find(".modal-body").html("<img src=\"/images/cargando.gif\" />");

        if ($(this).hasClass("propuesta")) {
            $target.find(".modal-body").html("<iframe width=\"100%\" height=\"450px\" src=\"" + href + "\"></iframe>");
        } else {
            $.ajax({
                url: href,
                content: "text/html; charset=utf-8",
                success: function (d) {
                    $target.find(".modal-body").html(d);
                },
                error: function (xhr, textStatus, errorThrown) {
                    $target.find(".modal-body").html(textStatus);
                }
            });
        }

        if ($this.is('a')) e.preventDefault()

        $target.one('show.bs.modal', function (showEvent) {
            if (showEvent.isDefaultPrevented()) return // only register focus restorer if modal will actually get shown
            $target.one('hidden.bs.modal', function () {
                $this.is(':visible') && $this.trigger('focus')
            })
        })
        //Plugin.call($target, option, this)
        return false;
    });

    //comprobamos todas las validaciones contra blanco para mostrar asterisco o no..
    $(".validation[validation-regex]").each(function () {
        //obtenemos los valores...necesitamos escapar el selector en el momento que tenemos el caracter !
        var regex = $(this).attr("validation-regex");
        if (!new RegExp(regex).test("")) {
            $(this).closest(".row").find(".asterisco").removeClass("hidden");
        }
    });

    /*
     $('li.acc-wizard-todo a').on('click', function (e) {  //Step 1
        $('html,body').animate({
            scrollTop: $('.panel-body[data-step=2a]').offset().top
        }, 'slow');
    });

    */



    $(document).on('click', '.acc-wizard-sidebar [data-toggle="collapse"]', function (e) {
        var step = $(this).attr("data-step");
        var $panel = $('.panel[data-step=' + step + ']');

        //var $listado = $('.acc-wizard-sidebar').find('li');
        //$listado.removeClass('acc-wizard-active');
        //$(this).parent().addClass('acc-wizard-active');

        //$(this).closest('.acc-wizard-sidebar').find('li').removeClass('acc-wizard-active');
        $(this).closest('li').toggleClass('acc-wizard-active').siblings('li').removeClass('acc-wizard-active');

        /*$('html,body').animate({
            scrollTop: $panel.offset().top - 176 //Hay que hacer un pequeño offset por los fijos que se están utilizando..
        }, 'slow');*/

        setTimeout(function () {
            $('html,body').animate({
                scrollTop: $panel.offset().top - 200 //Hay que hacer un pequeño offset por los fijos que se están utilizando..
            }, 'slow');
        }, 500);

    });

    $(document).on('click', '.panel a[data-toggle="collapse"]', function (e) {
        var step = $(this).closest('div.panel').attr("data-step");
        $('.acc-wizard-sidebar [data-toggle="collapse"][data-step=' + step + ']').trigger('click'); /*.click()*/

        /*
        var $this = $(this);
        var $listado = $('.acc-wizard-sidebar').find('li');
        
        if ($listado.hasClass('acc-wizard-active')) {
            $listado.removeClass('acc-wizard-active');
        }

        var $posicionListado = $listado.find('a[data-step=' + step + ']');
        $posicionListado.parent().addClass('acc-wizard-active');

        setTimeout(function () {
            $('html,body').animate({
                scrollTop: $this.offset().top - 235 //Hay que hacer un pequeño offset por los fijos que se están utilizando..
            }, 'slow');
        }, 500);
        */

    });

    /*$(document).on('click','#generar-propuesta-Retailer',function (e){
        alert('pulsaste');
    });*/

    /**
     * Manipulamos los select para la visualización de elementos
     * */
    $(document).on('change', '.acc-wizard select', function (e) {
        var name = $(this).attr("name");
        var value = $(this).val();
        $("[class*='" + name + "']").hide();
        $("[class*='" + name + "'][class*='" + value + "']").show();
        //tenemos que hacer combos anidados...
        $("[class*='" + name + "'][class*='" + value + "'] select").trigger("change");

    });

    $(".acc-wizard select").trigger("change");

    var $myGroup = $('.acc-wizard');
    $myGroup.on('show.bs.collapse', '.collapse', function () {
        $myGroup.find('.collapse.in').collapse('hide');
    });

    // Simulación de generar propuesta y pasos

    $('li.content-generar-propuesta').on('click', function () {
        var $this = $(this);
        var $PasoDescargarPropuesta = $('.acc-wizard-sidebar .pdf-propuesta');
        $this.find('img').removeClass('hidden');
        setTimeout(function () {
            if($PasoDescargarPropuesta.hasClass('hidden')){
                $PasoDescargarPropuesta.removeClass('hidden');
                // $PasoDescargarPropuesta.addClass('error');
            }
            else if ($PasoDescargarPropuesta.hasClass('error')){
                $PasoDescargarPropuesta.removeClass('error');
                $PasoDescargarPropuesta.addClass('hidden');
            }
            else if (!$PasoDescargarPropuesta.hasClass('hidden')){
                $PasoDescargarPropuesta.addClass('error');
            }
             else{
                /* ******* */
            }
            $this.find('img').addClass('hidden');
        }, 2000);
    });

});